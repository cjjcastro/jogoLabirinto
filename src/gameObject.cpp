#include <iostream>
#include <string>
#include "gameObject.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

gameObject::gameObject(){}

gameObject::~gameObject(){}

gameObject::gameObject(char sprite, int posx, int posy){
	this->sprite = sprite;
	this->pos_x = posx;
	this->pos_y = posy;
}

void gameObject::setPos_x(int valor){

	this->pos_x = valor;

}

void gameObject::setPos_y(int valor){
	this->pos_y = valor;
}

void gameObject::setSprite(char sprite){
	this->sprite = sprite;
}

int gameObject::getPos_x(){
	return this->pos_x;
}

int gameObject::getPos_y(){
	return this->pos_y;
}

char gameObject::getSprite(){
	return this->sprite;
}

void gameObject::movimento(){

	char direcao = 'l';

	direcao = getch();

	if(direcao == 'w'){
		this->setPos_y(-1);
	} else if (direcao == 's'){
		this->setPos_y(1);
	} else if (direcao == 'a'){
		this->setPos_x(-1);
	} else if (direcao == 'd'){
		this->setPos_x(1);
	}

}
