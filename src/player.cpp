#include <iostream>
#include <string>
#include "player.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;


Player::Player(){
	this->sprite = '@';
	this->pos_x = 1;
	this->pos_y = 1;
	this->alive = 5;
	this->score = 0;
	this->winner = false;
}

void Player::setPos_x(int valor){

	this->pos_x += valor;

}

void Player::setPos_y(int valor){
	this->pos_y += valor;
}

void Player::setSprite(char sprite){
	this->sprite = sprite;
}

void Player::setAlive(){
	this->alive--;
}

void Player::setScore(int valor){
	this->score += valor;
}

void Player::setWinner(bool win){
	this->winner = TRUE;
}

int Player::getPos_x(){
	return this->pos_x;
}

int Player::getPos_y(){
	return this->pos_y;
}

char Player::getSprite(){
	return this->sprite;
}

int Player::getAlive(){
	return this->alive;
}

int Player::getScore(){
	return this->score;
}

bool Player::getWinner(){
	return this->winner;
}

void Player::movimento(char w, char s, char a, char d){

	char direcao = 'l';
	int muda = 0;

	direcao = getch();

	if(direcao == 'w' || direcao == 'W'){
		if(w == '='){muda = 1;}	else{
			this->setPos_y(-1);
		}
	} else if (direcao == 's' || direcao == 'S'){
		if(s == '='){muda = 1;}	else{
			this->setPos_y(1);
		}
	} else if (direcao == 'a' || direcao == 'A'){
		if(a == '='){muda = 1;}	else{
			this->setPos_x(-1);
		}
	} else if (direcao == 'd' || direcao == 'D'){
		if(d == '='){muda = 1;}	else{
			this->setPos_x(+1);
		}
	}

	while(muda == 1){

		direcao = getch();

		if(direcao == 'w' || direcao == 'W'){
			if(w == '='){muda = 1;}	else{
				this->setPos_y(-1);
				muda = 0;
			}
		} else if (direcao == 's' || direcao == 'S'){
			if(s == '='){muda = 1;}	else{
				this->setPos_y(+1);
				muda = 0;
			}
		} else if (direcao == 'a' || direcao == 'A'){
			if(a == '='){muda = 1;}	else{
				this->setPos_x(-1);
				muda = 0;
			}
		} else if (direcao == 'd' || direcao == 'D'){
			if(d == '='){muda = 1;}	else{
				this->setPos_x(+1);
				muda = 0;
			}
		}
	}
}
