#include <iostream>
#include <string>
#include "Map.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

Map::Map(){}

void Map::setRange(){

	ifstream map ("maze_map.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(map, aux);
		for(int u = 0; u < 50; u++){
			this->range[i][u] = aux[u];
			this->rangeB[i][u] = aux[u];
		}
	}

	map.close();
}

void Map::getRange(){

		for(int i = 0; i < 20; i++){
			for(int u = 0; u < 50; u++){
				printw("%c", this->range[i][u]);
			}
		printw("\n");
		}
}

void Map::cleanRange(){

	for(int i = 0; i < 20; i++){
		for(int u = 0; u < 50; u++){
			this->range[i][u] = this->rangeB[i][u];
		}
	}
}

void Map::addElemento(char sprite, int posx, int posy){

	this->range[posy][posx] = sprite;

}

void Map::YouLose(){
	ifstream maplose ("you_lose.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(maplose, aux);
		for(int u = 0; u < 50; u++){
			this->youLose[i][u] = aux[u];
			printw("%c", this->youLose[i][u]);
		}
		printw("\n");
	}

	maplose.close();
}

void Map::YouWin(){
	ifstream mapwin ("you_win.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapwin, aux);
		for(int u = 0; u < 50; u++){
			this->youWin[i][u] = aux[u];
			printw("%c", this->youWin[i][u]);
		}
		printw("\n");
	}

	mapwin.close();
}

char Map::retornaElemento(int posx, int posy){
	return this->rangeB[posx][posy];
}

void Map::TelaInicial(){
	ifstream mapinicial ("tela_inicial.txt");

	string aux;

	for(int i = 0; i < 20; i++){
		getline(mapinicial, aux);
		for(int u = 0; u < 50; u++){
			this->telaInicial[i][u] = aux[u];
			printw("%c", this->telaInicial[i][u]);
		}
		printw("\n");
	}

	mapinicial.close();
}
