#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "Map.hpp"
#include "player.hpp"
#include "trap.hpp"
#include "bonus.hpp"

using namespace std;



int main(int argc, char **argv){

	Map * mapa = new Map();
	mapa->setRange();

	Player * player = new Player();
	Trap * trap[20];
	for(int i = 0; i < 20; i++){
		trap[i] = new Trap();
		trap[i]-> movimento();
		while(mapa->retornaElemento(trap[i]->getPos_y(), trap[i]->getPos_x()) == '='){
			trap[i]-> movimento();
		}
	}
	Bonus * bonus[5];
	for(int i = 0; i < 5; i++){
		bonus[i] = new Bonus();
		bonus[i]-> movimento();
		while(mapa->retornaElemento(bonus[i]->getPos_y(), bonus[i]->getPos_x()) == '='){
			bonus[i]-> movimento();
		}
	}

	int tempo = 0;
	char teclado = 'a';

	initscr();
	clear();
	mapa ->TelaInicial();
	teclado = getch();

	while(TRUE){
		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();

		//ADICIONA AS ARMADILHAS NO MAPA
		for(int i = 0; i < 20; i++){
			mapa->addElemento(trap[i]->getSprite(), trap[i]->getPos_x(), trap[i]->getPos_y());
			if(tempo == 4){
				trap[i]->movimento();
				while(mapa->retornaElemento(trap[i]->getPos_y(), trap[i]->getPos_x()) == '='){
					trap[i]-> movimento();
				}
			}
		}

		//ADICIONA OS BONUS AO MAPA
		for(int i = 0; i < 5; i++){
			mapa->addElemento(bonus[i]->getSprite(), bonus[i]->getPos_x(), bonus[i]->getPos_y());
			if(tempo == 4 ){
				bonus[i]->movimento();
				while(mapa->retornaElemento(bonus[i]->getPos_y(), bonus[i]->getPos_x()) == '='){
					bonus[i]-> movimento();
				}
				tempo = 0;
			}
		}

		//ADICIONA O JOGADOR AO MAPA
		mapa->addElemento(player->getSprite(), player->getPos_x(), player->getPos_y());

		//IMPRIME O MAPA NO TERMINAL
		mapa->getRange();

		tempo++;

		printw("VIDA = %d      CHAVES DO INFINITO = %d", player->getAlive(), player->getScore());

		//MOVIMENTO DO PLAYER
		player->movimento(mapa->retornaElemento(player->getPos_y() -1, player->getPos_x()),
											mapa->retornaElemento(player->getPos_y() +1, player->getPos_x()),
											mapa->retornaElemento(player->getPos_y(), player->getPos_x() -1),
											mapa->retornaElemento(player->getPos_y(), player->getPos_x() +1));

		//COLISÃO COM AS ARMADILHAS
		for(int i = 0; i < 20; i++){
			if(player->getPos_x() == trap[i]->getPos_x() && player->getPos_y() == trap[i]->getPos_y()){
				player->setAlive();
			}
		}
		//CONDIÇÃO DE DERROTA
		if(player->getAlive() == 0){
			clear();
			mapa->YouLose();
			teclado = getch();
			endwin();
			break;
		}

		//COLISÃO COM OS BÔNUS
		for(int i = 0; i < 5 ;i++){
			if(player->getPos_x() == bonus[i]->getPos_x() && player->getPos_y() == bonus[i]->getPos_y()){
				player->setScore(1);
			}
		}

		//CONDIÇÃO DE VITÓRIA
		if(player->getPos_x() == 49 && player->getPos_y() == 16 && player->getScore()>=3){
			clear();
			mapa->YouWin();
			teclado = getch();
			endwin();
			break;
		}

		//LIMPANDO O MAPA
		mapa->cleanRange();

		refresh();
		endwin();
	}

	delete(mapa);
	delete(player);
	for(int i = 0; i < 20; i++){
		delete(trap[i]);
	}
	for(int i = 0; i < 5; i++){
		delete(bonus[i]);
	}

	return 0;
}
