#include <iostream>
#include <string>
#include "trap.hpp"
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include <ctime>
#include <cstdlib>

using namespace std;


Trap::Trap(){
	this->sprite = '$';
	this->pos_x = 0;
	this->pos_y = 0;
  this->damage = 1;
}

void Trap::setPos_x(int valor){

	this->pos_x = valor;

}

void Trap::setPos_y(int valor){
	this->pos_y = valor;
}

void Trap::setSprite(char sprite){
	this->sprite = sprite;
}

void Trap::setDamage(int damage){
  this->damage = damage;
}

int Trap::getPos_x(){
  return this->pos_x;
}

int Trap::getPos_y(){
  return this->pos_y;
}

char Trap::getSprite(){
  return this->sprite;
}

int Trap::getDamage(){
  return this->damage;
}

void Trap::movimento() {
  int x, y;
  x = rand() % 50;
  y = rand() % 20;
  this->pos_x = x;
  this->pos_y = y;
}
