Exercício de Programação 1 - O Labirinto
nome: Cleber José de Castro Júnior
matrícula: 16/0025834

O jogo consiste em um labirinto cujo objetivo é chegar no portal '8' tendo
pegado no mínimo três chaves do infinito '+';

A movimentação do jogo é:
w/W - movimenta para cima;
s/S - movimenta para baixo;
a/A - movimenta para a esquerda;
d/D - movimenta para a direita;

Classes:
gameObject - Serve como classe abstrata das classes player, trap e bonus;
player - É a classe responsável pelo player, a vida, a movimentação e todos os
aspectos do player;
trap - É a classe responsável pelas traps, a movimentação;
bonus - É a classe responsável pelos bônus, a movimentação;
Map - É a classe responsável por instanciar e imprimir o mapa, e todas as telas do
 jogo, e adicionar elementos no mapa;


Compilação: Para compilar basta utilizar o MakeFile abrindo o terminal, navegando
até a pasta do jogo e digitar o comando 'make' em seguida para execuatar o jogo,
digite no terminal o comando 'make run';

Elementos do jogo:
Jogador - É representado pelo caracter '@', começa com 5 de vida;
chaves do infinito - É representado pelo caracter '+', aparecem aleatoriamente e
para passar pelo portal é necessário ter no mínimo 3 chaves;
fantasmas - É representado pelo caracter '$', aparecem aleatoriamente e tiram 1
de vida quado colidem com o jogador;
paredes - É representado pelo caracter '=', e não pode ser atravessado por nenhum,
elemento do jogo;
portal do infinito - É representado pelo caracter '8', e é a condição de vitória
quando o player já tem no mínimo 3 chaves do infinito;
