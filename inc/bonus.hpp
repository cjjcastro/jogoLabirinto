#ifndef BONUS_H
#define BONUS_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Bonus : public gameObject {

	private:
    int score;


	public:
		Bonus();

		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
    void setScore(int score);

		int getPos_x();
		int getPos_y();
		char getSprite();
    int getScore();

		void movimento();

};

#endif
