#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <iostream>
#include <string>

class gameObject{

	protected:
		char sprite;
		int pos_x;
		int pos_y;

	public:
		gameObject();
		virtual ~gameObject();
		gameObject(char sprite, int posx, int posy);

		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
		int getPos_x();
		int getPos_y();
		char getSprite();

		void movimento();

};

#endif
