#ifndef MAP_H
#define MAP_H

#include <iostream>
#include <string>

class Map{

	private:
		char range[20][50];
		char rangeB[20][50];
		char youLose[20][50];
		char youWin[20][50];
		char telaInicial[20][50];

	public:
		Map();

		void setRange();
		void getRange();
		void cleanRange();
		void addElemento(char sprite, int posx, int posy);
		void YouLose();
		void YouWin();
		void TelaInicial();

		char retornaElemento(int posx, int posy);

};

#endif
