#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Player : public gameObject {

	private:
		int alive;
		int score;
		bool winner;

	public:
		Player();

		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
		void setAlive();
		void setScore(int valor);
		void setWinner(bool win);

		int getPos_x();
		int getPos_y();
		char getSprite();
		int getAlive();
		int getScore();
		bool getWinner();

		void movimento(char w, char s, char a, char d);

};

#endif
