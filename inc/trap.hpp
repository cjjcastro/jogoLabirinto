#ifndef TRAP_H
#define TRAP_H

#include <iostream>
#include <string>
#include "gameObject.hpp"

class Trap : public gameObject {

	private:
    int damage;


	public:
		Trap();

		void setPos_x(int valor);
		void setPos_y(int valor);
		void setSprite(char sprite);
    void setDamage(int damage);

		int getPos_x();
		int getPos_y();
		char getSprite();
    int getDamage();

		void movimento();

};

#endif
